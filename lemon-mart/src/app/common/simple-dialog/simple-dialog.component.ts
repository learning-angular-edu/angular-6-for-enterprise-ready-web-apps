import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  templateUrl: './simple-dialog.component.html'
})
export class SimpleDialogComponent {
  constructor(public dialogRef: MatDialogRef<SimpleDialogComponent, Boolean>,
              @Inject(MAT_DIALOG_DATA) public data: any) {}
}
