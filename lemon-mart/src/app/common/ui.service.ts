import {Injectable} from '@angular/core';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {MatSnackBarConfig} from '@angular/material/typings/esm5/snack-bar';
import {Observable} from 'rxjs';
import {SimpleDialogComponent} from './simple-dialog/simple-dialog.component';

@Injectable()
export class UiService {
  constructor(private snackBar: MatSnackBar, private dialog: MatDialog) {}

  showToast(message: string, action = 'Close', config?: MatSnackBarConfig) {
    this.snackBar.open(message, action, config || {
      duration: 7000
    });
  }

  showDialog(title: string,
             content: string,
             okText = 'OK',
             cancelText?: string,
             customConfig?: MatDialogConfig): Observable<Boolean> {
    const dialogRef = this.dialog.open(SimpleDialogComponent, customConfig || {
      width: '300px',
      data: {title: title, content: content, okText: okText, cancelText: cancelText}
    });
    return dialogRef.afterClosed();
  }
}
