import {Component, OnInit} from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {ObservableMedia} from '@angular/flex-layout';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthService} from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  _displayAccountIcons = false;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private authService: AuthService, public media: ObservableMedia) {
    iconRegistry.addSvgIcon(
      'lemon',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/lemon.svg')
    );
  }

  ngOnInit() {
    this.authService.authStatus.subscribe(authStatus => this._displayAccountIcons = authStatus.isAuthenticated);
  }

  get displayAccountIcons() {
    return this._displayAccountIcons;
  }
}
